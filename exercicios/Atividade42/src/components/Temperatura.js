import React from 'react'

export default class Temperatura extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            temp: 25
        }
    }

    tempmais = () => {
        this.setState({ temp: this.state.temp + 1 })
    }

    tempmenos = () => {
        this.setState({ temp: this.state.temp - 1 })
    }

    render() {
        return (<div>Temperatura:
            <button onClick={this.tempmenos}>&lt;</button>
            {this.state.temp}
            <button onClick={this.tempmais}>&gt;</button></div>)
    }
}