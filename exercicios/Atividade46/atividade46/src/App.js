import React from 'react';
import './App.css';
import Infos from './components/Infos';

function App() {
  return (
    <div className="App">
      <Infos></Infos>
    </div>)
}

export default App;