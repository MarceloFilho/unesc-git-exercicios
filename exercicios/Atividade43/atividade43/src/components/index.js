var express = require('express');
var app = express();
const fs = require('fs')
const cors = require('cors')
app.use(cors())
app.use(express.json())

app.get('/computador', function (req, res) {
    res.json(JSON.parse(fs.readFileSync("computadores.json", 'utf8')))
})

app.post('/computador', function (req, res) {
    var file = JSON.parse(fs.readFileSync('computadores.json', 'utf8'));
    file.push(req.body);
    fs.writeFileSync('computadores.json', JSON.stringify(file), { encoding: 'utf-8', flag: 'w' });
    res.send()
})

app.delete('/computador/:id', (req, res) => {
    var lista = JSON.parse(fs.readFileSync('computadores.json', 'utf8'))
    lista.splice(req.params.id, 1)
    fs.writeFileSync('computadores.json', JSON.stringify(lista), { encoding: 'utf-8', flag: 'w' });
    res.send()
})

app.listen(3001, function () {
    console.log("Aplicação rodando na porta 3001")
})
